use super::strong_types::{Name, LB, UB};
use crate::model::model_error::ModelError;
use crate::model::setter_traits::{BoundSetter, IdSetter, NameSetter};

#[derive(Copy, Debug, Clone, Eq, Hash, PartialEq)]
pub struct VariableId(i64);

impl VariableId {
    pub fn new(id: i64) -> Self {
        Self(id)
    }
}

pub struct Variable {
    id: VariableId,
    name: Name,
    lower_bound: LB,
    upper_bound: UB,
}

impl Variable {
    pub fn id(&self) -> VariableId {
        self.id
    }
    pub fn name(&self) -> &Name {
        &self.name
    }
    pub fn lower_bound(&self) -> LB {
        self.lower_bound
    }
    pub fn upper_bound(&self) -> UB {
        self.upper_bound
    }
}

impl IdSetter for Variable {
    type IdType = VariableId;
    fn set_id(&mut self, id: Self::IdType) {
        self.id = id;
    }
}

impl NameSetter for Variable {
    fn set_name(&mut self, name: &Name) {
        self.name = name.clone();
    }
}

impl Default for Variable {
    fn default() -> Self {
        Self {
            id: VariableId::new(0),
            name: Name::new(&String::from("")),
            lower_bound: LB::new(0.0),
            upper_bound: UB::new(f64::MAX),
        }
    }
}

impl BoundSetter for Variable {
    fn set_bounds(&mut self, lower_bound: LB, upper_bound: UB) -> Result<(), ModelError> {
        if lower_bound.value > upper_bound.value {
            return Err(ModelError::new(
                "Lower Bound hast to be smaller or equal to upper_bound",
            ));
        }

        self.lower_bound = lower_bound;
        self.upper_bound = upper_bound;

        Ok(())
    }

    fn set_lower_bound(&mut self, lower_bound: LB) -> Result<(), ModelError> {
        if lower_bound.value > self.upper_bound.value {
            return Err(ModelError::new(
                "New lower Bound hast to be smaller or equal to upper_bound",
            ));
        }
        self.lower_bound = lower_bound;

        Ok(())
    }

    fn set_upper_bound(&mut self, upper_bound: UB) -> Result<(), ModelError> {
        if self.lower_bound.value > upper_bound.value {
            return Err(ModelError::new(
                "New Upperbound has to be greater or equal to lower bound",
            ));
        }
        self.upper_bound = upper_bound;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_variable_id_new() {
        let id_value = 10;
        let variable_id_new = VariableId::new(id_value);
        let variable_id_direct = VariableId(id_value);

        assert_eq!(variable_id_new, variable_id_direct);
    }

    #[test]
    fn test_variable_default() {
        let variable = Variable::default();

        // Use a match pattern to get the inner values of VariableId, Name, LB, and UB
        assert_eq!(variable.id, VariableId::new(0));

        assert_eq!(variable.name, Name::new(&String::from("")));

        assert_eq!(variable.lower_bound, LB::new(0.0));

        assert_eq!(variable.upper_bound, UB::new(f64::MAX));
    }

    #[test]
    fn test_set_id() {
        let mut variable = Variable::default();

        let new_id = VariableId(3);
        variable.set_id(new_id);

        assert_eq!(variable.id, new_id);
    }

    #[test]
    fn test_set_name() {
        let mut variable = Variable::default();
        let name = Name::new(&("Tester".to_string()));
        variable.set_name(&name);

        assert_eq!(name, variable.name);
    }

    #[test]
    fn test_set_bounds() {
        let mut variable = Variable::default();
        let lower_bound = LB::new(1.3);
        let upper_bound = UB::new(5.3);
        variable
            .set_bounds(lower_bound, upper_bound)
            .expect("Bounds invalid!");

        assert_eq!(variable.lower_bound, lower_bound);
        assert_eq!(variable.upper_bound, upper_bound);
    }

    #[test]
    fn test_set_lower_bound() {
        let mut variable: Variable = Variable::default();
        let lower_bound = LB::new(1.0);
        variable
            .set_lower_bound(lower_bound)
            .expect("Lower bound invalid!");

        assert_eq!(variable.lower_bound, lower_bound);
    }

    #[test]
    fn test_set_upper_bound() {
        let mut variable: Variable = Variable::default();
        let upper_bound = UB::new(20.0);
        variable
            .set_upper_bound(upper_bound)
            .expect("Upper bound invalid!");

        assert_eq!(variable.upper_bound, upper_bound);
    }

    #[test]
    fn test_set_bounds_error() {
        let mut variable = Variable::default();
        let lower_bound = LB::new(22.0);
        let upper_bound = UB::new(-3.0);

        match variable.set_bounds(lower_bound, upper_bound) {
            Err(msg) => assert_eq!(
                msg.to_string(),
                "Lower Bound hast to be smaller or equal to upper_bound"
            ),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_set_lower_bound_error() {
        let mut variable = Variable::default();
        variable.upper_bound = UB::new(3.0);
        let lower_bound = LB::new(4.0);

        match variable.set_lower_bound(lower_bound) {
            Err(msg) => assert_eq!(
                msg.to_string(),
                "New lower Bound hast to be smaller or equal to upper_bound"
            ),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_set_upper_bound_error() {
        let mut variable = Variable::default();
        let upper_bound = UB::new(-5.0);

        match variable.set_upper_bound(upper_bound) {
            Err(msg) => assert_eq!(
                msg.to_string(),
                "New Upperbound has to be greater or equal to lower bound"
            ),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_variable_id() {
        let mut variable = Variable::default();
        variable.set_id(VariableId(5));
        let id = variable.id();
        assert_eq!(id, VariableId(5));
    }

    #[test]
    fn test_variable_name() {
        let mut variable = Variable::default();
        let name_input = Name::new(&"Testor".to_string());
        variable.set_name(&name_input);
        let name = variable.name();
        assert_eq!(*name, name_input);
    }

    #[test]
    fn test_variable_lower_bound() {
        let variable = Variable::default();
        let lower_bound = variable.lower_bound();
        assert_eq!(lower_bound, LB::new(0.0));
    }

    #[test]
    fn test_variable_upper_bound() {
        let variable = Variable::default();
        let upper_bound = variable.upper_bound();
        assert_eq!(upper_bound, UB::new(f64::MAX));
    }
}
