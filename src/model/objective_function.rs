use crate::model::container_traits::Empty;
use crate::model::model_error::ModelError;
use crate::model::setter_traits::{ObjectiveTypeSetter, VariableCoefficientSetter};
use crate::model::variable::VariableId;
use std::collections::HashMap;

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum ObjectiveType {
    Minimization,
    Maximization,
}

pub struct ObjectiveFunction {
    objective_type: ObjectiveType,
    coefficients_by_variable_id: HashMap<VariableId, f64>,
}

impl Default for ObjectiveFunction {
    fn default() -> Self {
        ObjectiveFunction {
            objective_type: ObjectiveType::Maximization,
            coefficients_by_variable_id: HashMap::new(),
        }
    }
}

impl ObjectiveFunction {
    pub fn objective_type(&self) -> ObjectiveType {
        self.objective_type
    }
}

impl Empty for ObjectiveFunction {
    fn is_empty(&self) -> bool {
        self.coefficients_by_variable_id.is_empty()
    }
}

impl ObjectiveTypeSetter for ObjectiveFunction {
    type T = ObjectiveType;

    fn set_objective_type(&mut self, objective_type: Self::T) {
        self.objective_type = objective_type
    }
}

impl VariableCoefficientSetter for ObjectiveFunction {
    type IdType = VariableId;

    fn set_coefficient(&mut self, id: Self::IdType, coefficient: f64) {
        self.coefficients_by_variable_id.insert(id, coefficient);
    }

    fn set_coefficients(
        &mut self,
        ids: &Vec<Self::IdType>,
        coefficients: &Vec<f64>,
    ) -> Result<(), ModelError> {
        if ids.len() != coefficients.len() {
            return Err(ModelError::new(
                "The lengths of ids and coefficients are not the same",
            ));
        }

        for (id, coefficient) in ids.into_iter().zip(coefficients.into_iter()) {
            self.coefficients_by_variable_id
                .insert(id.clone(), coefficient.clone());
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_objective_function_default() {
        let objective_function = ObjectiveFunction::default();

        assert_eq!(
            objective_function.objective_type,
            ObjectiveType::Maximization
        );
        assert!(objective_function.coefficients_by_variable_id.is_empty());
    }

    #[test]
    fn test_set_objective_type() {
        let mut objective_function = ObjectiveFunction::default();
        let objective_type = ObjectiveType::Minimization;
        objective_function.set_objective_type(objective_type);

        assert_eq!(objective_function.objective_type, objective_type);
    }

    #[test]
    fn test_set_coefficient() {
        let mut objective_function = ObjectiveFunction::default();

        let variable_id = VariableId::new(3);
        let coefficient = 5.4;
        objective_function.set_coefficient(variable_id, coefficient);

        assert_eq!(
            objective_function.coefficients_by_variable_id[&variable_id],
            coefficient
        );
    }

    #[test]
    fn test_set_coefficients() {
        let mut objective_function = ObjectiveFunction::default();

        let variable_ids = vec![VariableId::new(5), VariableId::new(3)];
        let coefficients = vec![-3.0, 193.254];
        assert!(objective_function
            .set_coefficients(&variable_ids, &coefficients)
            .is_ok());

        for (variable_id, coefficient) in variable_ids.into_iter().zip(coefficients.into_iter()) {
            assert_eq!(
                objective_function.coefficients_by_variable_id[&variable_id],
                coefficient
            );
        }
    }

    #[test]
    fn test_set_coefficients_error() {
        let mut objective_function = ObjectiveFunction::default();

        let variable_ids = vec![VariableId::new(7)];
        let coefficients = vec![0.3, 2.6];
        assert!(objective_function
            .set_coefficients(&variable_ids, &coefficients)
            .is_err());
    }

    #[test]
    fn test_set_coefficients_error_2() {
        let mut objective_function = ObjectiveFunction::default();

        let variable_ids = vec![
            VariableId::new(3),
            VariableId::new(7),
            VariableId::new(2),
            VariableId::new(10),
        ];
        let coefficients = vec![-2.0, 5.0, -3.0];
        assert!(objective_function
            .set_coefficients(&variable_ids, &coefficients)
            .is_err());
    }

    #[test]
    fn test_objective_type() {
        let objective_function = ObjectiveFunction::default();

        assert_eq!(
            objective_function.objective_type(),
            ObjectiveType::Maximization
        );
    }

    #[test]
    fn test_is_empty() {
        let mut objective_function = ObjectiveFunction::default();

        assert!(objective_function.is_empty());

        let variable_id = VariableId::new(3);
        let coefficient = 5.4;
        objective_function.set_coefficient(variable_id, coefficient);

        assert!(!objective_function.is_empty());
    }
}
