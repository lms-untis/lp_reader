use std::fmt;

#[derive(Debug, PartialEq)]
pub struct ModelError {
    details: String,
}

impl ModelError {
    pub fn new(msg: &str) -> ModelError {
        ModelError {
            details: msg.to_string(),
        }
    }
}

impl fmt::Display for ModelError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_model_error_new() {
        let msg = "Test error";
        let error = ModelError::new(msg);

        assert_eq!(error.details, msg);
    }

    #[test]
    fn test_model_error_description() {
        let msg = "Test error";
        let error = ModelError::new(msg);

        assert_eq!(error.to_string(), msg);
    }

    #[test]
    fn test_model_details() {
        let details = "Test error";
        let error = ModelError {
            details: details.into(),
        };

        assert_eq!(error.details, details);
    }
}
