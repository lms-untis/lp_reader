use super::strong_types::{Name, LB, UB};
use crate::model::model_error::ModelError;

pub trait IdSetter {
    type IdType;
    fn set_id(&mut self, id: Self::IdType);
}

pub trait NameSetter {
    fn set_name(&mut self, id: &Name);
}

pub trait RightHandSideSetter {
    fn set_right_hand_side_value(&mut self, value: f64);
}

pub trait RelationalOperatorSetter {
    type Operator;

    fn set_relational_operator(&mut self, relational_operator: Self::Operator);
}

pub trait VariableCoefficientSetter {
    type IdType;

    fn set_coefficient(&mut self, id: Self::IdType, coefficient: f64);
    fn set_coefficients(
        &mut self,
        ids: &Vec<Self::IdType>,
        coefficients: &Vec<f64>,
    ) -> Result<(), ModelError>;
}

pub trait BoundSetter {
    fn set_bounds(&mut self, lower_bound: LB, upper_bound: UB) -> Result<(), ModelError>;
    fn set_lower_bound(&mut self, lower_bound: LB) -> Result<(), ModelError>;
    fn set_upper_bound(&mut self, upper_bound: UB) -> Result<(), ModelError>;
}

pub trait ObjectiveTypeSetter {
    type T;
    fn set_objective_type(&mut self, objective_type: Self::T);
}
