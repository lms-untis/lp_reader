use crate::model::constraint::ConstraintId;
use crate::model::model_error::ModelError;
use crate::model::variable::VariableId;

pub trait LPReader {
    fn add_variable(&mut self, variable_name: &String) -> Result<VariableId, ModelError>;
    fn add_variable_with_bounds(
        &mut self,
        lp_variable_string: &String,
    ) -> Result<VariableId, ModelError>;

    fn add_objective_function(
        &mut self,
        lp_objective_function_string: &String,
    ) -> Result<(), ModelError>;

    fn add_constraint(&mut self, lp_constraint_string: &String)
        -> Result<ConstraintId, ModelError>;

    fn read_lp(&mut self, lp_string: &String) -> Result<(), ModelError>;

    fn read_lp_file(&mut self, file_path: &String) -> Result<(), ModelError>;
}

use regex::Regex;

pub fn is_valid_variable_name(name: &str) -> bool {
    let re_start = Regex::new(r"^[0-9.]").unwrap();
    if re_start.is_match(name) {
        return false;
    }

    let re = Regex::new(r###"^[a-zA-Z0-9!"#$%&(),.;?@_‘’{}~]{1,255}$"###).unwrap();
    re.is_match(name)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_name() {
        assert!(is_valid_variable_name("validName"));
        assert!(is_valid_variable_name("valid_name"));
        assert!(is_valid_variable_name("valid.name"));
        assert!(is_valid_variable_name("valid!name"));
        assert!(is_valid_variable_name("valid\"name"));
        assert!(is_valid_variable_name("valid#name"));
        assert!(is_valid_variable_name("valid$name"));
        assert!(is_valid_variable_name("valid%name"));
        assert!(is_valid_variable_name("valid&name"));
        assert!(is_valid_variable_name("valid(name"));
        assert!(is_valid_variable_name("valid)name"));
        assert!(is_valid_variable_name("valid,name"));
        assert!(is_valid_variable_name("valid.name"));
        assert!(is_valid_variable_name("valid;name"));
        assert!(is_valid_variable_name("valid?name"));
        assert!(is_valid_variable_name("valid@name"));
        assert!(is_valid_variable_name("valid‘name"));
        assert!(is_valid_variable_name("valid’name"));
        assert!(is_valid_variable_name("valid{name"));
        assert!(is_valid_variable_name("valid}name"));
        assert!(is_valid_variable_name("valid~name"));
        assert!(is_valid_variable_name("vali3name"));
        assert!(is_valid_variable_name("vali1~name"));
        assert!(is_valid_variable_name("vali5name"));
    }

    #[test]
    fn test_invalid_name() {
        assert!(!is_valid_variable_name("invalid name"));
        assert!(!is_valid_variable_name("valid-name"));
        assert!(!is_valid_variable_name("invalid/name"));
        assert!(!is_valid_variable_name("invalid*name"));
        assert!(!is_valid_variable_name("invalid+name"));
        assert!(!is_valid_variable_name("invalid=name"));
        assert!(!is_valid_variable_name("invalid[name"));
        assert!(!is_valid_variable_name("invalid]name"));
        assert!(!is_valid_variable_name("invalid^name"));
        assert!(!is_valid_variable_name("invalid`name"));
        assert!(!is_valid_variable_name("invalid|name"));
        assert!(!is_valid_variable_name("invalid<name"));
        assert!(!is_valid_variable_name("invalid>name"));
        assert!(!is_valid_variable_name("invalid:name"));
        assert!(!is_valid_variable_name("3invalid"));
        assert!(!is_valid_variable_name("2invalid"));
        assert!(!is_valid_variable_name(".invalid"));
    }
}