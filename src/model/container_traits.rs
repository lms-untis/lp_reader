use crate::model::constraint::{Constraint, ConstraintId, RelationalOperatorType};
use crate::model::model_error::ModelError;
use crate::model::strong_types::{Name, LB, UB};
use crate::model::variable::{Variable, VariableId};

pub trait GetVariable {
    fn get_mut_variable_by_id(&mut self, id: VariableId) -> Result<&mut Variable, ModelError>;
    fn get_mut_variable_by_name(&mut self, name: &Name) -> Result<&mut Variable, ModelError>;
    fn get_variable_by_id(&self, id: VariableId) -> Result<&Variable, ModelError>;
    fn get_variable_by_name(&self, name: &Name) -> Result<&Variable, ModelError>;
}

pub trait Contains {
    type Id;
    fn contains_by_id(&self, id: Self::Id) -> bool;
    fn contains_by_name(&self, name: &Name) -> bool;
}

pub trait VariableCreater {
    fn create_variable(
        &mut self,
        name: &Name,
        lower_bound: Option<LB>,
        upper_bound: Option<UB>,
    ) -> Result<&mut Variable, ModelError>;
}

pub trait ConstraintCreator {
    fn create_constraint(
        &mut self,
        name: &Name,
        relational_operator: RelationalOperatorType,
        right_hand_side_value: f64,
    ) -> Result<&mut Constraint, ModelError>;
}

pub trait GetConstraint {
    fn get_mut_constraint_by_id(&mut self, id: ConstraintId)
        -> Result<&mut Constraint, ModelError>;
    fn get_mut_constraint_by_name(&mut self, name: &Name) -> Result<&mut Constraint, ModelError>;
    fn get_constraint_by_id(&self, id: ConstraintId) -> Result<&Constraint, ModelError>;
    fn get_constraint_by_name(&self, name: &Name) -> Result<&Constraint, ModelError>;
}

pub trait Empty {
    fn is_empty(&self) -> bool;
}
