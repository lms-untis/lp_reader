use crate::model::constraint::ConstraintId;
use crate::model::constraints::Constraints;
use crate::model::container_traits::{Contains, GetVariable, VariableCreater};
use crate::model::lp_reader::{is_valid_variable_name, LPReader};
use crate::model::model_error::ModelError;
use crate::model::objective_function::ObjectiveFunction;
use crate::model::setter_traits::{BoundSetter, NameSetter};
use crate::model::strong_types::{LB, Name, UB};
use crate::model::variable::{Variable, VariableId};
use crate::model::variables::Variables;

pub struct Model {
    name: Name,
    pub variables: Variables,
    pub constraints: Constraints,
    pub objective_function: ObjectiveFunction,
}

impl Model {
    pub fn new(name: Name) -> Self {
        Self {
            name,
            variables: Variables::default(),
            constraints: Constraints::default(),
            objective_function: ObjectiveFunction::default(),
        }
    }
    pub fn name(&self) -> &Name {
        &self.name
    }
}

impl Default for Model {
    fn default() -> Self {
        Self {
            name: Name::new(&String::from("")),
            variables: Variables::default(),
            constraints: Constraints::default(),
            objective_function: ObjectiveFunction::default(),
        }
    }
}

impl NameSetter for Model {
    fn set_name(&mut self, name: &Name) {
        self.name = name.clone();
    }
}

impl LPReader for Model {
    fn add_variable(&mut self, variable_name: &String) -> Result<VariableId, ModelError> {
        if !is_valid_variable_name(variable_name)
        {
            return Err(ModelError::new("Invalid variable name"));
        }
        let name = Name::new(variable_name);
        if self.variables.contains_by_name(&name) {
            return Ok(self.variables.get_variable_by_name(&name).unwrap().id())
        }
        Ok(self.variables.create_variable(&name, None, None).unwrap().id())
    }

    fn add_variable_with_bounds(&mut self, lp_variable_string: &String) -> Result<VariableId, ModelError> {
        let split: Vec<&str> = lp_variable_string.split("<=").collect();
        let split_no_whitespace: Vec<String> = split.iter().map(|s| s.replace(" ", "")).collect();
        if split_no_whitespace.len() != 3 {
            return Err(ModelError::new("Invalid variable string"));
        }
        if !is_valid_variable_name(split_no_whitespace[1].as_str())
        {
            return Err(ModelError::new("Invalid variable name"));
        }
        let name = Name::new(&split_no_whitespace[1]);
        let lower_bound = LB::new(split_no_whitespace[0].parse::<f64>().unwrap());
        let upper_bound = UB::new(split_no_whitespace[2].parse::<f64>().unwrap());
        if self.variables.contains_by_name(&name) {
            let variable: &mut Variable = self.variables.get_mut_variable_by_name(&name).unwrap();
            variable.set_bounds(lower_bound, upper_bound).expect("Bounds invalid");
            return Ok(variable.id());
        }
        let variable = self.variables.create_variable(&name, Some(lower_bound), Some(upper_bound)).expect("Bounds invalid");
        Ok(variable.id())
    }

    fn add_objective_function(&mut self, lp_objective_function_string: &String) -> Result<(), ModelError> {
        todo!()
    }

    fn add_constraint(&mut self, lp_constraint_string: &String) -> Result<ConstraintId, ModelError> {
        todo!()
    }

    fn read_lp(&mut self, lp_string: &String) -> Result<(), ModelError> {
        todo!()
    }

    fn read_lp_file(&mut self, file_path: &String) -> Result<(), ModelError> {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::container_traits::Empty;
    use crate::model::objective_function::ObjectiveType;

    #[test]
    fn test_set_name() {
        let mut model = Model::default();
        let name = Name::new(&("Tester".to_string()));
        model.set_name(&name);

        assert_eq!(name, model.name);
    }

    #[test]
    fn test_model_new() {
        let name = Name::new(&("TestModel".to_string()));
        let model = Model::new(name.clone());

        assert_eq!(&name, model.name());
        assert!(model.variables.is_empty());
        assert!(model.constraints.is_empty());
        assert!(model.objective_function.is_empty());
        assert_eq!(
            model.objective_function.objective_type(),
            ObjectiveType::Maximization
        );
    }
    #[test]
    fn test_model_default() {
        let name = Name::new(&("".to_string()));
        let model = Model::default();

        assert_eq!(&name, model.name());
        assert!(model.variables.is_empty());
        assert!(model.constraints.is_empty());
        assert!(model.objective_function.is_empty());
        assert_eq!(
            model.objective_function.objective_type(),
            ObjectiveType::Maximization
        );
    }

    #[test]
    fn test_model_name() {
        let name = Name::new(&("Ski".to_string()));
        let mut model = Model::new(name.clone());

        assert_eq!(&name, model.name());

        let name2 = Name::new(&("Tester".to_string()));
        model.set_name(&name2);

        assert_eq!(&name2, model.name());
    }

    #[test]
    fn test_add_variable() {
        let mut model = Model::default();

        // Test with a valid variable name
        let variable_name_valid = String::from("validName");
        let result = model.add_variable(&variable_name_valid);
        assert!(result.is_ok());

        // Test with an invalid variable name
        let variable_name_invalid = String::from("invalid name");
        match model.add_variable(&variable_name_invalid) {
            Err(msg) => assert_eq!(msg.to_string(), "Invalid variable name"),
            _ => panic!("Test failed"),
        }
        
        let variable_2 = model.add_variable(&variable_name_valid).unwrap();
        assert_eq!(result.unwrap(), variable_2);
    }

    #[test]
    fn test_add_variable_with_bounds_valid_new() {
        let mut model = Model::default();

        // Test with a valid variable string
        let variable_string = String::from("0.3 <= validName <= 100");
        let result = model.add_variable_with_bounds(&variable_string);
        assert!(result.is_ok());
        let variable_id = result.unwrap();
        let variable : &Variable = model.variables.get_variable_by_id(variable_id).unwrap();
        assert_eq!(variable.lower_bound(), LB::new(0.3));
        assert_eq!(variable.upper_bound(), UB::new(100.0));
        assert_eq!(variable.name(), &Name::new(&String::from("validName")));
    }

    #[test]
    fn test_add_variable_with_bounds_valid_change_of_bounds() {
        let mut model = Model::default();

        // Test with a valid variable string
        let variable_string = String::from("0.3 <= validName <= 100");
        let result = model.add_variable_with_bounds(&variable_string);
        assert!(result.is_ok());
        let variable_id = result.unwrap();

        let variable_string_2 = String::from("-0.3 <= validName <= 83");
        let result_2 = model.add_variable_with_bounds(&variable_string_2);
        assert!(result_2.is_ok());
        let variable_id_2 = result_2.unwrap();
        
        assert_eq!(variable_id,variable_id_2);

        let variable : &Variable = model.variables.get_variable_by_id(variable_id).unwrap();
        assert_eq!(variable.lower_bound(), LB::new(-0.3));
        assert_eq!(variable.upper_bound(), UB::new(83.0));
        assert_eq!(variable.name(), &Name::new(&String::from("validName")));
    }
}
