use crate::model::constraint::{Constraint, ConstraintId, RelationalOperatorType};
use crate::model::container_traits::{ConstraintCreator, Contains, Empty, GetConstraint};
use crate::model::model_error::ModelError;
use crate::model::setter_traits::{
    IdSetter, NameSetter, RelationalOperatorSetter, RightHandSideSetter,
};
use crate::model::strong_types::Name;
use std::collections::HashMap;

#[derive(Default)]
pub struct Constraints {
    constraints: Vec<Constraint>,
    constraint_index_by_id: HashMap<ConstraintId, i64>,
    constraint_index_by_name: HashMap<Name, i64>,
}

impl ConstraintCreator for Constraints {
    fn create_constraint(
        &mut self,
        name: &Name,
        relational_operator: RelationalOperatorType,
        right_hand_side_value: f64,
    ) -> Result<&mut Constraint, ModelError> {
        if self.constraint_index_by_name.contains_key(name) {
            return Err(ModelError::new("Constraint name already exists"));
        }
        let new_index = self.constraints.len() as i64;

        let constraint_id = ConstraintId::new(new_index);
        let mut new_constraint = Constraint::default();
        new_constraint.set_id(constraint_id);
        new_constraint.set_name(&name);
        new_constraint.set_relational_operator(relational_operator);
        new_constraint.set_right_hand_side_value(right_hand_side_value);

        self.constraints.push(new_constraint);
        self.constraint_index_by_id.insert(constraint_id, new_index);
        self.constraint_index_by_name
            .insert(name.clone(), new_index);
        Ok(&mut self.constraints[new_index as usize])
    }
}

impl Contains for Constraints {
    type Id = ConstraintId;

    fn contains_by_id(&self, id: Self::Id) -> bool {
        self.constraint_index_by_id.contains_key(&id)
    }

    fn contains_by_name(&self, name: &Name) -> bool {
        self.constraint_index_by_name.contains_key(&name)
    }
}

impl GetConstraint for Constraints {
    fn get_mut_constraint_by_id(
        &mut self,
        id: ConstraintId,
    ) -> Result<&mut Constraint, ModelError> {
        if !self.constraint_index_by_id.contains_key(&id) {
            return Err(ModelError::new("Constraint does not exist"));
        }
        Ok(&mut self.constraints[self.constraint_index_by_id[&id] as usize])
    }

    fn get_mut_constraint_by_name(&mut self, name: &Name) -> Result<&mut Constraint, ModelError> {
        if !self.constraint_index_by_name.contains_key(&name) {
            return Err(ModelError::new("Constraint does not exist"));
        }
        Ok(&mut self.constraints[self.constraint_index_by_name[&name] as usize])
    }

    fn get_constraint_by_id(&self, id: ConstraintId) -> Result<&Constraint, ModelError> {
        if !self.constraint_index_by_id.contains_key(&id) {
            return Err(ModelError::new("Constraint does not exist"));
        }
        Ok(&self.constraints[self.constraint_index_by_id[&id] as usize])
    }

    fn get_constraint_by_name(&self, name: &Name) -> Result<&Constraint, ModelError> {
        if !self.constraint_index_by_name.contains_key(&name) {
            return Err(ModelError::new("Constraint does not exist"));
        }
        Ok(&self.constraints[self.constraint_index_by_name[&name] as usize])
    }
}

impl Empty for Constraints {
    fn is_empty(&self) -> bool {
        self.constraints.is_empty()
            && self.constraint_index_by_id.is_empty()
            && self.constraint_index_by_name.is_empty()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_constraint_error() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("Michie".to_string()));
        let relational_operator = RelationalOperatorType::GreaterThanOrEqual;
        let right_hand_side_value = 1.0;

        constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();
        match constraints.create_constraint(&name, relational_operator, right_hand_side_value) {
            Err(msg) => assert_eq!(msg.to_string(), "Constraint name already exists"),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_create_constraint_success() {
        let mut constraints = Constraints::default();
        let name_1 = Name::new(&("Turn".to_string()));
        let relational_operator_1 = RelationalOperatorType::LessThanOrEqual;
        let right_hand_side_value_1 = 1.0;

        let constraint = constraints
            .create_constraint(&name_1, relational_operator_1, right_hand_side_value_1)
            .unwrap();

        assert_eq!(constraint.name(), &name_1);
        assert_eq!(constraint.id(), ConstraintId::new(0));
        assert_eq!(constraint.relational_operator(), relational_operator_1);
        assert_eq!(constraint.right_hand_side_value(), right_hand_side_value_1);
    }

    #[test]
    fn test_create_two_constraint_success() {
        let mut constraints = Constraints::default();
        let name_1 = Name::new(&("Turn".to_string()));
        let relational_operator_1 = RelationalOperatorType::LessThanOrEqual;
        let right_hand_side_value_1 = 1.0;

        let _ = constraints
            .create_constraint(&name_1, relational_operator_1, right_hand_side_value_1)
            .unwrap();

        let name_2 = Name::new(&("Burn".to_string()));
        let relational_operator_2 = RelationalOperatorType::Equal;
        let right_hand_side_value_2 = 1.0;

        let constraint2 = constraints
            .create_constraint(&name_2, relational_operator_2, right_hand_side_value_2)
            .unwrap();

        assert_eq!(constraint2.name(), &name_2);
        assert_eq!(constraint2.id(), ConstraintId::new(1));
        assert_eq!(constraint2.relational_operator(), relational_operator_2);
        assert_eq!(constraint2.right_hand_side_value(), right_hand_side_value_2);
    }

    #[test]
    fn test_contains_by_id() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("Test".to_string()));
        let relational_operator = RelationalOperatorType::Equal;
        let right_hand_side_value = 1.0;

        let constraint = constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();
        let id = constraint.id();

        assert!(constraints.contains_by_id(id));

        let non_existing_id = ConstraintId::new(999);

        assert!(!constraints.contains_by_id(non_existing_id));
    }

    #[test]
    fn test_contains_by_name() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("Turn".to_string()));
        let relational_operator = RelationalOperatorType::Equal;
        let right_hand_side_value = 1.0;

        let _ = constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();
        assert!(constraints.contains_by_name(&name));

        let non_existing_name = Name::new(&("Ball".to_string()));

        assert!(!constraints.contains_by_name(&non_existing_name));
    }

    #[test]
    fn test_get_mut_constraint_by_id() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("Caroline".to_string()));
        let name2 = Name::new(&("karo".to_string()));
        let right_hand_side_value = 1.0;
        let relational_operator = RelationalOperatorType::GreaterThanOrEqual;

        let constraint = constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();
        let id = constraint.id();

        let constraint = constraints.get_mut_constraint_by_id(id).unwrap();
        assert_eq!(constraint.name(), &name);
        constraint.set_name(&name2);
        assert_eq!(constraint.name(), &name2);
    }

    #[test]
    fn test_get_mut_constraint_by_name() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("tau".to_string()));
        let right_hand_side_value = 1.0;
        let relational_operator = RelationalOperatorType::LessThanOrEqual;

        let _ = constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();

        let constraint = constraints.get_mut_constraint_by_name(&name).unwrap();
        assert_eq!(constraint.relational_operator(), relational_operator);
        assert_eq!(constraint.right_hand_side_value(), right_hand_side_value);
        let _ = constraint.set_right_hand_side_value(4.3);
        assert_eq!(constraint.right_hand_side_value(), 4.3);
    }

    #[test]
    fn test_get_constraint_by_id() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("bau".to_string()));
        let right_hand_side_value = 1.0;
        let relational_operator = RelationalOperatorType::GreaterThanOrEqual;

        let constraint = constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();
        let id = constraint.id();

        assert_eq!(constraints.get_constraint_by_id(id).unwrap().name(), &name);
    }

    #[test]
    fn test_get_constraint_by_name() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("barbara".to_string()));
        let right_hand_side_value = 1.0;
        let relational_operator = RelationalOperatorType::GreaterThanOrEqual;

        let _ = constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();

        assert_eq!(
            constraints.get_constraint_by_name(&name).unwrap().name(),
            &name
        );
    }

    #[test]
    fn test_get_mut_constraint_by_name_exception() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("Schulz".to_string()));

        match constraints.get_mut_constraint_by_name(&name) {
            Err(msg) => assert_eq!(msg, ModelError::new("Constraint does not exist")),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_get_constraint_by_name_exception() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("bau".to_string()));
        let right_hand_side_value = 1.0;
        let relational_operator = RelationalOperatorType::GreaterThanOrEqual;

        let _ = constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();
        let name2 = Name::new(&("Shall".to_string()));

        match constraints.get_constraint_by_name(&name2) {
            Err(msg) => assert_eq!(msg, ModelError::new("Constraint does not exist")),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_get_mut_constraint_by_id_exception() {
        let constraints = Constraints::default();
        let id = ConstraintId::new(5);

        match constraints.get_constraint_by_id(id) {
            Err(msg) => assert_eq!(msg, ModelError::new("Constraint does not exist")),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_get_constraint_by_id_exception() {
        let mut constraints = Constraints::default();
        let name = Name::new(&("Tamas".to_string()));
        let right_hand_side_value = 1.0;
        let relational_operator = RelationalOperatorType::GreaterThanOrEqual;

        let _ = constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();
        let id = ConstraintId::new(3);

        match constraints.get_mut_constraint_by_id(id) {
            Err(msg) => assert_eq!(msg, ModelError::new("Constraint does not exist")),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_is_empty() {
        let mut constraints = Constraints::default();
        assert!(constraints.is_empty());

        let name = Name::new(&("TestConstraint".to_string()));
        let relational_operator = RelationalOperatorType::LessThanOrEqual;
        let right_hand_side_value = 5.0;

        let _ = constraints
            .create_constraint(&name, relational_operator, right_hand_side_value)
            .unwrap();
        assert!(!constraints.is_empty());
    }
}
