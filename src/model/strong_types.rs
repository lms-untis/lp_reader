#[derive(Debug, Clone, Eq, Hash, PartialEq)]
pub struct Name(String);

impl Name {
    pub fn new(name: &String) -> Self {
        Self(name.clone())
    }
}

#[derive(Copy, Debug, Clone, PartialEq)]
pub struct LB {
    pub value: f64,
}

impl LB {
    pub fn new(value: f64) -> Self {
        LB { value }
    }
}
#[derive(Copy, Debug, Clone, PartialEq)]
pub struct UB {
    pub value: f64,
}

impl UB {
    pub fn new(value: f64) -> Self {
        UB { value }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_name_new() {
        let name_string = "Test323".to_string();
        let name = Name::new(&name_string);
        let name_direct = Name(name_string);

        assert_eq!(name, name_direct);
    }

    #[test]
    fn test_lb_new() {
        let lb_value: f64 = 10.3;
        let lb_new = LB::new(lb_value);

        assert_eq!(lb_new.value, lb_value);
    }

    #[test]
    fn test_ub_new() {
        let ub_value: f64 = -10.5;
        let ub_new = UB::new(ub_value);

        assert_eq!(ub_new.value, ub_value);
    }
}
