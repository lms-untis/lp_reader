use crate::model::container_traits::{Contains, Empty, GetVariable, VariableCreater};
use crate::model::model_error::ModelError;
use crate::model::setter_traits::{BoundSetter, IdSetter, NameSetter};
use crate::model::strong_types::{Name, LB, UB};
use crate::model::variable::{Variable, VariableId};
use std::collections::HashMap;

#[derive(Default)]
pub struct Variables {
    variables: Vec<Variable>,
    variable_index_by_id: HashMap<VariableId, i64>,
    variable_index_by_name: HashMap<Name, i64>,
}

impl VariableCreater for Variables {
    fn create_variable(
        &mut self,
        name: &Name,
        lower_bound: Option<LB>,
        upper_bound: Option<UB>,
    ) -> Result<&mut Variable, ModelError> {
        if self.variable_index_by_name.contains_key(name) {
            return Err(ModelError::new("Variable already exists"));
        }

        let new_index = self.variables.len() as i64;
        let variable_id = VariableId::new(new_index);
        let mut new_variable = Variable::default();
        new_variable.set_id(variable_id);
        new_variable.set_name(name);
        new_variable.set_bounds(lower_bound.unwrap_or(LB::new(0.0)), upper_bound.unwrap_or(UB::new(f64::MAX)))?;

        self.variables.push(new_variable);
        self.variable_index_by_id.insert(variable_id, new_index);
        self.variable_index_by_name.insert(name.clone(), new_index);
        Ok(&mut self.variables[new_index as usize])
    }
}

impl Empty for Variables {
    fn is_empty(&self) -> bool {
        self.variables.is_empty()
            && self.variable_index_by_id.is_empty()
            && self.variable_index_by_name.is_empty()
    }
}

impl GetVariable for Variables {
    fn get_mut_variable_by_id(&mut self, id: VariableId) -> Result<&mut Variable, ModelError> {
        if !self.variable_index_by_id.contains_key(&id) {
            return Err(ModelError::new("Variable does not exist"));
        }
        Ok(&mut self.variables[self.variable_index_by_id[&id] as usize])
    }

    fn get_mut_variable_by_name(&mut self, name: &Name) -> Result<&mut Variable, ModelError> {
        if !self.variable_index_by_name.contains_key(&name) {
            return Err(ModelError::new("Variable does not exist"));
        }
        Ok(&mut self.variables[self.variable_index_by_name[&name] as usize])
    }

    fn get_variable_by_id(&self, id: VariableId) -> Result<&Variable, ModelError> {
        if !self.variable_index_by_id.contains_key(&id) {
            return Err(ModelError::new("Variable does not exist"));
        }
        Ok(&self.variables[self.variable_index_by_id[&id] as usize])
    }

    fn get_variable_by_name(&self, name: &Name) -> Result<&Variable, ModelError> {
        if !self.variable_index_by_name.contains_key(&name) {
            return Err(ModelError::new("Variable does not exist"));
        }
        Ok(&self.variables[self.variable_index_by_name[&name] as usize])
    }
}

impl Contains for Variables {
    type Id = VariableId;

    fn contains_by_id(&self, id: Self::Id) -> bool {
        self.variable_index_by_id.contains_key(&id)
    }

    fn contains_by_name(&self, name: &Name) -> bool {
        self.variable_index_by_name.contains_key(&name)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_variable_success() {
        let mut variables = Variables::default();
        let name = Name::new(&("xa".to_string()));
        let lower_bound = LB::new(5.0);
        let upper_bound = UB::new(12.5);

        let variable = variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();

        assert_eq!(*variable.name(), name);
        assert_eq!(variable.lower_bound(), lower_bound);
        assert_eq!(variable.upper_bound(), upper_bound);
    }

    #[test]
    fn test_add_variable_duplicate_name() {
        let mut variables = Variables::default();
        let name = Name::new(&("tutu".to_string()));
        let lower_bound = LB::new(0.0);
        let upper_bound = UB::new(1.0);

        variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();

        match variables.create_variable(&name, Option::from(lower_bound), Option::from(upper_bound)) {
            Err(msg) => assert_eq!(msg.to_string(), "Variable already exists"),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_add_variable_invalid_bounds() {
        let mut variables = Variables::default();
        let name = Name::new(&("bass".to_string()));
        let lower_bound = LB::new(-50.0);
        let upper_bound = UB::new(-65.0);

        match variables.create_variable(&name, Option::from(lower_bound), Option::from(upper_bound)) {
            Err(msg) => assert_eq!(
                msg.to_string(),
                "Lower Bound hast to be smaller or equal to upper_bound"
            ),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_get_mut_variable_by_id() {
        let mut variables = Variables::default();
        let name = Name::new(&("Caroline".to_string()));
        let name2 = Name::new(&("karo".to_string()));
        let lower_bound = LB::new(0.0);
        let upper_bound = UB::new(1.0);

        let variable = variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();
        let id = variable.id();

        let variable = variables.get_mut_variable_by_id(id).unwrap();
        assert_eq!(variable.name(), &name);
        variable.set_name(&name2);
        assert_eq!(variable.name(), &name2);
    }

    #[test]
    fn test_get_mut_variable_by_name() {
        let mut variables = Variables::default();
        let name = Name::new(&("tau".to_string()));
        let lower_bound = LB::new(2.0);
        let upper_bound = UB::new(5.0);

        let _ = variables.create_variable(&name, Option::from(lower_bound), Option::from(upper_bound));

        let variable = variables.get_mut_variable_by_name(&name).unwrap();
        assert_eq!(variable.upper_bound(), upper_bound);
        assert_eq!(variable.lower_bound(), lower_bound);
        let _ = variable.set_lower_bound(LB::new(5.0));
        assert_eq!(variable.lower_bound(), LB::new(5.0));
    }

    #[test]
    fn test_get_variable_by_id() {
        let mut variables = Variables::default();
        let name = Name::new(&("bau".to_string()));
        let lower_bound = LB::new(-1.0);
        let upper_bound = UB::new(3.0);

        let variable = variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();
        let id = variable.id();

        assert_eq!(variables.get_variable_by_id(id).unwrap().name(), &name);
    }

    #[test]
    fn test_get_variable_by_name() {
        let mut variables = Variables::default();
        let name = Name::new(&("barbara".to_string()));
        let lower_bound = LB::new(0.0);
        let upper_bound = UB::new(1.0);

        variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();

        assert_eq!(variables.get_variable_by_name(&name).unwrap().name(), &name);
    }

    #[test]
    fn test_get_mut_variable_by_name_exception() {
        let mut variables = Variables::default();
        let name = Name::new(&("Schulz".to_string()));

        match variables.get_mut_variable_by_name(&name) {
            Err(msg) => assert_eq!(msg, ModelError::new("Variable does not exist")),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_get_variable_by_name_exception() {
        let mut variables = Variables::default();
        let name = Name::new(&("bau".to_string()));
        let lower_bound = LB::new(-1.0);
        let upper_bound = UB::new(3.0);
        variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();
        let name2 = Name::new(&("Shall".to_string()));

        match variables.get_variable_by_name(&name2) {
            Err(msg) => assert_eq!(msg, ModelError::new("Variable does not exist")),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_get_mut_variable_by_id_exception() {
        let variables = Variables::default();
        let id = VariableId::new(5);

        match variables.get_variable_by_id(id) {
            Err(msg) => assert_eq!(msg, ModelError::new("Variable does not exist")),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_get_variable_by_id_exception() {
        let mut variables = Variables::default();
        let name = Name::new(&("Tamas".to_string()));
        let lower_bound = LB::new(-5.0);
        let upper_bound = UB::new(4.0);
        variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();
        let id = VariableId::new(3);

        match variables.get_mut_variable_by_id(id) {
            Err(msg) => assert_eq!(msg, ModelError::new("Variable does not exist")),
            _ => panic!("Test failed"),
        }
    }

    #[test]
    fn test_contains_by_id_true() {
        let mut variables = Variables::default();
        let name = Name::new(&("Turn".to_string()));
        let lower_bound = LB::new(0.0);
        let upper_bound = UB::new(1.0);

        let variable = variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();
        let id = variable.id();

        assert!(variables.contains_by_id(id));
    }

    #[test]
    fn test_contains_by_id_false() {
        let mut variables = Variables::default();
        let name = Name::new(&("Turn".to_string()));
        let lower_bound = LB::new(0.0);
        let upper_bound = UB::new(1.0);
        let _ = variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();
        let id = VariableId::new(5);

        assert!(!variables.contains_by_id(id));
    }

    #[test]
    fn test_contains_by_name_true() {
        let mut variables = Variables::default();
        let name = Name::new(&("Klara".to_string()));
        let lower_bound = LB::new(0.0);
        let upper_bound = UB::new(1.0);

        variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();

        assert!(variables.contains_by_name(&name));
    }

    #[test]
    fn test_contains_by_name_false() {
        let variables = Variables::default();
        let name = Name::new(&("Milo".to_string()));

        assert!(!variables.contains_by_name(&name));
    }

    #[test]
    fn test_is_empty() {
        let mut variables = Variables::default();
        assert!(variables.is_empty());

        let name = Name::new(&("TestVariable".to_string()));
        let lower_bound = LB::new(0.0);
        let upper_bound = UB::new(1.0);

        let _ = variables
            .create_variable(&name, Option::from(lower_bound), Option::from(upper_bound))
            .unwrap();
        assert!(!variables.is_empty());
    }
}
