use super::setter_traits::{
    IdSetter, NameSetter, RelationalOperatorSetter, RightHandSideSetter, VariableCoefficientSetter,
};
use super::strong_types::Name;
use super::variable::VariableId;
use crate::model::model_error::ModelError;
use std::collections::HashMap;

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum RelationalOperatorType {
    Equal,
    GreaterThanOrEqual,
    LessThanOrEqual,
}

#[derive(Copy, Debug, Clone, Eq, Hash, PartialEq)]
pub struct ConstraintId(i64);
impl ConstraintId {
    pub fn new(id: i64) -> Self {
        Self(id)
    }
}
pub struct Constraint {
    id: ConstraintId,
    name: Name,
    variable_coefficient_map: HashMap<VariableId, f64>,
    relational_operator: RelationalOperatorType,
    right_hand_side_value: f64,
}

impl Constraint {
    pub fn id(&self) -> ConstraintId {
        self.id
    }
    pub fn name(&self) -> &Name {
        &self.name
    }
    pub fn relational_operator(&self) -> RelationalOperatorType {
        self.relational_operator
    }
    pub fn right_hand_side_value(&self) -> f64 {
        self.right_hand_side_value
    }
}

impl Default for Constraint {
    fn default() -> Self {
        Self {
            id: ConstraintId(0),
            name: Name::new(&String::from("")),
            variable_coefficient_map: HashMap::new(),
            relational_operator: RelationalOperatorType::Equal,
            right_hand_side_value: 0.0,
        }
    }
}

impl IdSetter for Constraint {
    type IdType = ConstraintId;
    fn set_id(&mut self, id: Self::IdType) {
        self.id = id;
    }
}

impl NameSetter for Constraint {
    fn set_name(&mut self, name: &Name) {
        self.name = name.clone();
    }
}

impl RightHandSideSetter for Constraint {
    fn set_right_hand_side_value(&mut self, value: f64) {
        self.right_hand_side_value = value;
    }
}

impl RelationalOperatorSetter for Constraint {
    type Operator = RelationalOperatorType;

    fn set_relational_operator(&mut self, relational_operator: Self::Operator) {
        self.relational_operator = relational_operator;
    }
}

impl VariableCoefficientSetter for Constraint {
    type IdType = VariableId;

    fn set_coefficient(&mut self, id: Self::IdType, coefficient: f64) {
        self.variable_coefficient_map.insert(id, coefficient);
    }

    fn set_coefficients(
        &mut self,
        ids: &Vec<Self::IdType>,
        coefficients: &Vec<f64>,
    ) -> Result<(), ModelError> {
        if ids.len() != coefficients.len() {
            return Err(ModelError::new(
                "The lengths of ids and coefficients are not the same",
            ));
        }

        for (id, coefficient) in ids.into_iter().zip(coefficients.into_iter()) {
            self.variable_coefficient_map
                .insert(id.clone(), coefficient.clone());
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_set_id() {
        let mut constraint = Constraint::default();

        let new_id = ConstraintId(1);
        constraint.set_id(new_id);

        assert_eq!(constraint.id, new_id);
    }

    #[test]
    fn test_set_name() {
        let mut constraint = Constraint::default();
        let name = Name::new(&("Tester".to_string()));
        constraint.set_name(&name);

        assert_eq!(name, constraint.name);
    }

    #[test]
    fn test_set_right_hand_side_value() {
        let mut constraint = Constraint::default();

        let new_value = 10.0;
        constraint.set_right_hand_side_value(new_value);

        assert_eq!(constraint.right_hand_side_value, new_value);
    }

    #[test]
    fn test_set_relational_operator_1() {
        let mut constraint = Constraint::default();

        let new_operator = RelationalOperatorType::LessThanOrEqual;
        constraint.set_relational_operator(new_operator);

        assert_eq!(constraint.relational_operator, new_operator);
    }

    #[test]
    fn test_set_relational_operator_2() {
        let mut constraint = Constraint::default();

        let new_operator = RelationalOperatorType::GreaterThanOrEqual;
        constraint.set_relational_operator(new_operator);

        assert_eq!(constraint.relational_operator, new_operator);
    }

    #[test]
    fn test_set_coefficient() {
        let mut constraint = Constraint::default();

        let variable_id = VariableId::new(1);
        let coefficient = 2.0;
        constraint.set_coefficient(variable_id, coefficient);

        assert_eq!(
            constraint.variable_coefficient_map[&variable_id],
            coefficient
        );
    }

    #[test]
    fn test_set_coefficients() {
        let mut constraint = Constraint::default();

        let variable_ids = vec![VariableId::new(1), VariableId::new(2)];
        let coefficients = vec![2.0, 3.0];
        assert!(constraint
            .set_coefficients(&variable_ids, &coefficients)
            .is_ok());

        for (variable_id, coefficient) in variable_ids.into_iter().zip(coefficients.into_iter()) {
            assert_eq!(
                constraint.variable_coefficient_map[&variable_id],
                coefficient
            );
        }
    }

    #[test]
    fn test_set_coefficients_error() {
        let mut constraint = Constraint::default();

        let variable_ids = vec![VariableId::new(1)];
        let coefficients = vec![2.0, 3.0];
        assert!(constraint
            .set_coefficients(&variable_ids, &coefficients)
            .is_err());
    }

    #[test]
    fn test_set_coefficients_error_2() {
        let mut constraint = Constraint::default();

        let variable_ids = vec![
            VariableId::new(1),
            VariableId::new(7),
            VariableId::new(3),
            VariableId::new(10),
        ];
        let coefficients = vec![-1.0, 3.0];
        assert!(constraint
            .set_coefficients(&variable_ids, &coefficients)
            .is_err());
    }

    #[test]
    fn test_constraint_id() {
        let mut constraint = Constraint::default();
        let id = constraint.id();
        assert_eq!(id, ConstraintId(0));
        constraint.set_id(ConstraintId(5));
        assert_eq!(constraint.id(), ConstraintId(5));
    }

    #[test]
    fn test_constraint_name() {
        let constraint = Constraint::default();
        let name = constraint.name();
        assert_eq!(name, &Name::new(&("".to_string())));
    }

    #[test]
    fn test_constraint_relational_operator() {
        let constraint = Constraint::default();
        let relational_operator = constraint.relational_operator();
        assert_eq!(relational_operator, RelationalOperatorType::Equal);
    }

    #[test]
    fn test_constraint_right_hand_side_value() {
        let constraint = Constraint::default();
        let right_hand_side_value = constraint.right_hand_side_value();
        assert_eq!(right_hand_side_value, 0.0);
    }
}
